[![Java Version](https://img.shields.io/badge/Java-20-blue)](https://www.oracle.com/java/technologies/javase/jdk20-archive-downloads.html)
[![Spring Version](https://img.shields.io/badge/Spring-3.1.2-brightgreen)](https://spring.io/blog/2012/09/17/spring-framework-3-1-2-released)
[![Docker Version](https://img.shields.io/badge/Docker-24.0.5-blue)](https://docs.docker.com/release-notes/docker-engine/24.0.5/)
[![Docker-Compose Version](https://img.shields.io/badge/Docker--Compose-v2.20.2-blue)](https://github.com/docker/compose/releases/tag/1.27.0)
[![Gradle Version](https://img.shields.io/badge/Gradle-8.2-blue)](https://docs.gradle.org/8.2/release-notes.html)
[![Grafana Version](https://img.shields.io/badge/Grafana-10.0.3-orange)](https://grafana.com/blog/2023/07/12/grafana-10.0-is-now-available/)
[![Grafana Loki Version](https://img.shields.io/badge/Grafana%20Loki-v2.8-lightgrey)](https://grafana.com/blog/2023/08/01/grafana-loki-2.8-released/)
[![Grafana Tempo Version](https://img.shields.io/badge/Grafana%20Tempo-v2.2-lightgrey)](https://grafana.com/blog/2023/07/17/grafana-tempo-2.2-released/)
[![Prometheus Version](https://img.shields.io/badge/Prometheus-2.46.0-red)](https://github.com/prometheus/prometheus/releases/tag/v2.46.0)

# Spring Boot 3 Observability

---

> **Observability** in software development means having the ability to understand and diagnose how a software system
> behaves in real-world situations.
> It involves collecting data like **logs**, **metrics**, and **traces** to monitor performance, detect issues, and find
> solutions quickly.
> Observability is crucial for maintaining and improving the reliability, performance, and overall quality of software
> applications.

## Key aspects of observability include:

- **Monitoring:** Monitoring means constantly gathering data from different parts of the software system, like logs,
  metrics, and user interactions. This data helps us see how the system is doing, how it's performing, and how it's
  behaving over time.
- **Logging:** Logging is the practice of recording specific events, activities, or messages in a structured format.
  Logs help developers and operators understand what's happening within the system, diagnose errors, and trace the flow
  of requests through different components.
- **Tracing:** Tracing involves tracking the flow of a single request or transaction as it moves through various
  components and services in a distributed system. Distributed tracing helps pinpoint bottlenecks, latency issues, and
  dependencies that might impact overall system performance.
- **Metrics:** Metrics are quantitative measurements that provide insight into the system's behavior and performance.
  They can include data like CPU usage, memory consumption, request rates, error rates, and response times. These
  metrics are essential for identifying trends, anomalies, and performance bottlenecks.
- **Alerting:** Observability systems often include alerting mechanisms that notify developers and operations teams when
  predefined thresholds or conditions are met. Alerts can help address issues promptly and prevent potential service
  disruptions.
- **Visualization:** To make sense of the collected data, observability tools offer visualization capabilities, such as
  dashboards and charts. These visual representations allow teams to quickly identify patterns, anomalies, and trends,
  aiding in the identification of performance issues.
- **Correlation and Analysis:** Observability tools enable users to correlate data from different sources, such as logs,
  metrics, and traces, to gain a holistic understanding of the system's behavior. This is crucial for diagnosing complex
  issues that span multiple components.
- **Proactive Problem Solving:** Observability promotes a proactive approach to identifying and addressing issues before
  they impact users. By closely monitoring and analyzing the system's behavior, teams can make informed decisions to
  optimize performance and enhance user experience.

Overall, observability aims to provide a comprehensive view of a software system's runtime behavior and performance,
helping developers, operations teams, and other stakeholders understand how the system behaves in real-world scenarios.
This understanding enables faster troubleshooting, efficient root cause analysis, and continuous improvement of software
applications.

## Project Tools

Project tools are declared in projects [docker compose file](./compose.yml).

- [Prometheus](https://prometheus.io) is an open-source monitoring and alerting toolkit designed to help you collect,
  store, query, and visualize time-series data related to the performance and health of your systems.
- [Grafana Loki](https://grafana.com/oss/loki) is a log aggregation system designed to store and query logs from all
  your applications and infrastructure.
- [Grafana Tempo](https://grafana.com/products/cloud/traces) provides an easy-to-use, highly scalable, and
  cost-effective distributed tracing backend. Not indexing the traces makes it possible to store orders of magnitude
  more trace data for the same cost, and removes the need for sampling.
- [Grafana](https://grafana.com/grafana) is a versatile visualization and monitoring tool that empowers users to create
  rich, interactive dashboards from various data sources, enabling better insights and decision-making based on metric
  and time-series data.

## How To Run Project

Prerequisites:

- **Docker** version: min *22.0.0* and **docker compose** version: min *v2.20.2* should be installed
- **Java** min version min *17*
- **Apache Benchmark**

1. Run project tools <br>
   ``docker compose up -d`` <br><br>
2. Run projects/apps <br>
   ``./gradlew :apps:app1:bootRun --args='--spring.docker.compose.enabled=false' && ./gradlew :apps:app2:bootRun --args='--spring.docker.compose.enabled=false'`` <br><br>
3. Simulate request with apache benchmark, cli command <br>
   ``ab -n 500 -c 500 http://localhost:8080/api/app1`` <br><br>
4. Click on the link to see all the metrics, logs and traces in grafana <br>
   1. [Grafana Home](http://localhost:3000) Username: *admin*, Password: *admin*
   2. [Grafana Metrics Dashboard ![Grafana Metrics Dashboard](./images/metrics-dashboard.png)](http://localhost:3000/d/b7be9669-85be-4c5b-9232-0c4275973e1a/metrics-aggregation-from-prometheus?orgId=1) With **Custom Metric** For App1Service Greeting Method, Observed **Max Execution Time**
   3. [Grafana Logs Dashboard ![Grafana Logs Dashboard](./images/loggs-dashboard.png)](http://localhost:3000/d/b0875df4-e3f0-4d57-a8a1-c7e0016db57a/logging?orgId=1)
   4. [Grafana Traces Dashboard ![Grafana Traces Dashboard](./images/traces-dashboard.png)](http://localhost:3000/d/a507e508-4a88-4d87-83a4-218d238e8201/traces?orgId=1)

## Project References

- [Observability in Distributed Systems](https://www.baeldung.com/distributed-systems-observability)
- [Observability with Spring Boot 3 (Baeldung)](https://www.baeldung.com/spring-boot-3-observability)
- [Observability with Spring Boot 3 (Spring)](https://spring.io/blog/2022/10/12/observability-with-spring-boot-3)
- [Spring Boot 3 Observability | Monitor Method & Service Performance | JavaTechie](https://www.youtube.com/watch?v=J2N1X11pYnY)
- [Grafana Loki and Promtail | Log Aggregation and Visualization](https://www.youtube.com/watch?v=x2usZVRnXK4&t=455s)
- [Logging in Spring Boot with Grafana Loki](https://piotrminkowski.com/2023/07/05/logging-in-spring-boot-with-loki)
- [Observability Docker Compose](https://github.com/ThomasVitale/spring-boot-multitenancy/blob/main/docker-compose.yml)
- [Spring Open Feign](https://docs.spring.io/spring-cloud-openfeign/docs/current/reference/html/#micrometer-support)