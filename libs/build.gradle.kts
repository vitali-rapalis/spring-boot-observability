plugins {
    id("spring.boot.observability.spring-library-conventions") apply (false)
}

subprojects {
    apply(plugin = "spring.boot.observability.spring-library-conventions")
}