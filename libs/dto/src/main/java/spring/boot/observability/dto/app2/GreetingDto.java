package spring.boot.observability.dto.app2;

public record GreetingDto(String message) {
}
