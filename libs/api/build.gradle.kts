dependencies {
  implementation("org.springframework.cloud:spring-cloud-starter-openfeign")
  api("io.github.openfeign:feign-micrometer:12.4")
  api(project(":libs:dto"))
}