package spring.boot.observability.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import spring.boot.observability.dto.app2.GreetingDto;

@FeignClient(name = "app2-client", url = "http://localhost:8081/api/app2")
public interface App2Client {

    @RequestMapping(method = RequestMethod.GET)
    GreetingDto getGreeting();
}
