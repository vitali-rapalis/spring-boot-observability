plugins {
    // Apply the java Plugin to add support for Java.
    id("java")
}

group = "com.vrapalis.www.spring-boot-observability"
version = "0.0.1"

configurations {
  compileOnly {
    extendsFrom(configurations.annotationProcessor.get())
  }
}

java {
  sourceCompatibility = JavaVersion.VERSION_19
}

repositories {
    // Use Maven Central for resolving dependencies.
    mavenCentral()
}


