package spring.boot.observability.app2;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import spring.boot.observability.dto.app2.GreetingDto;

@RestController
@RequestMapping("api/app2")
public class App2RestController {

    @GetMapping
    public ResponseEntity<GreetingDto> getGreetingsFromApp2() {
        return ResponseEntity.ok(new GreetingDto("Greetings From App2!"));
    }
}
