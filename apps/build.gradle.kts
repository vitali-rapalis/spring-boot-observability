plugins {
    id("spring.boot.observability.spring-application-conventions")
}

subprojects {
    apply(plugin = "spring.boot.observability.spring-application-conventions")

    dependencies {
        implementation(project(":libs:config"))
        implementation(project(":libs:api"))
        implementation(project(":libs:logging"))
    }
}