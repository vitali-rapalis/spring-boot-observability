package spring.boot.observability.api;

import io.micrometer.observation.Observation;
import io.micrometer.observation.ObservationHandler;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class App1PerformanceTrackerHandler implements ObservationHandler<Observation.Context> {
    @Override
    public void onStart(Observation.Context context) {
        log.info("Execution of {} starts", context.getName());
        context.put("time", System.currentTimeMillis());
    }

    @Override
    public void onError(Observation.Context context) {
        log.error("Execution error {}", context.getError().getMessage());
    }

    @Override
    public void onEvent(Observation.Event event, Observation.Context context) {
    }

    @Override
    public void onScopeOpened(Observation.Context context) {

    }

    @Override
    public void onScopeClosed(Observation.Context context) {

    }

    @Override
    public void onScopeReset(Observation.Context context) {

    }

    @Override
    public void onStop(Observation.Context context) {
        log.info("Execution of {} ends, duration: {}", context.getName(), Long.valueOf(System.currentTimeMillis() - context.getOrDefault("time", 0L)).toString());
    }

    @Override
    public boolean supportsContext(Observation.Context context) {
        return context.getName() != null ? context.getName().equals("app1Service") : false;
    }
}
