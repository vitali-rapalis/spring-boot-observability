package spring.boot.observability.api;

import com.github.loki4j.slf4j.marker.LabelMarker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("api/app1")
public class App1RestController {

    private final App1Service app1Service;

    @GetMapping
    public ResponseEntity<String> getGreetingsFromApp1() {
        var greetingDto = app1Service.getGreetings();
        LabelMarker marker = LabelMarker.of("greetingId", () -> UUID.randomUUID().toString());
        log.info(marker, "Greeting resolved");
        return ResponseEntity.ok(String.format("Greetings From App1! And %s", greetingDto));
    }
}
