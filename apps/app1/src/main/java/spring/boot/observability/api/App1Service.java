package spring.boot.observability.api;

import io.micrometer.observation.annotation.Observed;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import spring.boot.observability.dto.app2.GreetingDto;

@Slf4j
@Service
@Observed(name = "app1Service")
@RequiredArgsConstructor
public class App1Service {

    private final App2Client app2Client;

    GreetingDto getGreetings() {
        return app2Client.getGreeting();
    }
}
