package spring.boot.observability.api;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.observation.ObservationRegistry;
import io.micrometer.observation.aop.ObservedAspect;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Configuration
public class App1Configuration {

    @Value("${server.port}")
    private int serverPort;

    @Bean
    MeterRegistryCustomizer<MeterRegistry> metricsCommonTags() throws UnknownHostException {
        return registry -> {
            try {
                registry.config().commonTags("instance", InetAddress.getLocalHost().getHostAddress() + ":" + serverPort).commonTags("application", "App1");
            } catch (UnknownHostException e) {
                throw new RuntimeException(e);
            }
        };
    }

     @Bean
     ObservedAspect observedAspect(ObservationRegistry observationRegistry) {
        observationRegistry.observationConfig().observationHandler(new App1PerformanceTrackerHandler());
        return new ObservedAspect(observationRegistry);
    }
}
